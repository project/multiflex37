<?php if ($block->region == 'left'): ?>
  <!-- Navigation unit -->
  <?php if ($id == 1): ?>
    <div class="round-border-topright"></div>
    <?php if ($block->subject): ?>
    <h1 class="first"><?php print $block->subject ?></h1>
    <?php endif;?>
  <?php else: ?>
    <?php if ($block->subject): ?>
    <h1><?php print $block->subject ?></h1>
    <?php endif;?>
  <?php endif; ?>
    <p class="content"><?php print $block->content ?></p>

<?php elseif ($block->region == 'right'): ?>
  <!-- Subcontent unit -->
  <div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="subcontent-unit-border clear-block block block-<?php print $block->module ?>">
    <div class="round-border-topleft"></div><div class="round-border-topright"></div>
    <?php if ($block->subject): ?>
      <h1><?php print $block->subject ?></h1>
    <?php endif;?>
    <p class="content"><?php print $block->content ?></p>
  </div>

<?php else: ?>
  <div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">
    <?php if ($block->subject): ?>
      <h1><?php print $block->subject ?></h1>
    <?php endif;?>
    <p class="content"><?php print $block->content ?></p>
  </div>
<?php endif; ?>


