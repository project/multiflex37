<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<!--  Version: Multiflex-3 Update-7 / Layout-4             -->
<!--  Date:    January 15, 2007                            -->
<!--  Author:  Wolfgang                                    -->
<!--  License: Fully open source without restrictions.     -->
<!--           Please keep footer credits with a link to   -->
<!--           Wolfgang (www.1-2-3-4.info). Thank you!     -->
<!--  Modified for Drupal 5.0 by Christopher Doyle         -->

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <style type="text/css">@import "<?php print base_path() . path_to_theme() ?>/css/layout4_setup.css";</style>
  <style type="text/css">@import "<?php print base_path() . path_to_theme() ?>/css/layout4_text.css";</style>
  <style type="text/css">@import "<?php print base_path() . path_to_theme() ?>/css/style.css";</style>
  <style type="text/css">@import "<?php print base_path() . path_to_theme() ?>/css/site.css";</style>
</head>
<!-- Global IE fix to avoid layout crash when single word size wider than column width -->
<!--[if IE]><style type="text/css"> body {word-wrap: break-word;}</style><![endif]-->
<body<?php //print phptemplate_body_class($sidebar_left, $sidebar_right); ?>>
  <!-- Main Page Container -->
  <div class="page-container">

    <!-- For alternative headers START PASTE here -->

    <!-- A. HEADER -->      
    <div class="header">
      <!-- A.1 HEADER TOP -->
      <div class="header-top">
        <!-- Sitelogo and sitename -->
        <?php
          $site_name = check_plain($site_name);
          $base_path = check_url($base_path);
          $logo = check_url($logo);
          if ($site_name && $logo) {
            print "
        <a class=\"sitelogo\" href=\"$base_path\" title=\"$site_name\"><img src=\"$logo\" alt=\"$site_name\" id=\"logo\" /></a>";
          }
          print "
        <div class=\"sitename\">";
          print "
          <h1><a href=\"$base_path\" title=\"$site_name\">$site_name</a></h1>
        </div>";
        ?>

        <!-- Navigation Level 0 -->
<!--        <div class="nav0">
          <ul>
            <li><a href="#" title="Pagina home in Italiano"><img src="./img/flag_italy.gif" alt="Image description" /></a></li>
            <li><a href="#" title="Homepage auf Deutsch"><img src="./img/flag_germany.gif" alt="Image description" /></a></li>
            <li><a href="#" title="Hemsidan p&aring; svenska"><img src="./img/flag_sweden.gif" alt="Image description" /></a></li>
          </ul>
        </div>-->

        <!-- Navigation Level 1 -->
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'nav1')) ?>
        <?php endif; ?>
      </div>

      <!-- A.2 HEADER MIDDLE -->
      <div class="header-middle">
        <!-- Site message -->
        <div class="sitemessage">
          <?php if ($site_slogan): ?>
            <h1><?php print $site_slogan; ?></h1>
          <?php endif; ?>
          <?php $mission = variable_get('site_mission', ''); if ($mission): ?>
            <h2><?php print $mission; ?></h2>
          <?php endif; ?>
        </div>
      </div>

      <!-- A.3 HEADER BOTTOM -->
      <div class="header-bottom">
        <!-- Navigation Level 2 (Drop-down menus) -->
        <div class="nav2">
          <!-- Navigation items -->
          <?php if (isset($secondary_links)) {
            // build two level menu for secondary links
            //TODO: there will almost certainly be an IE6/IE7 problem with
            // these menus... see the original template on how to fix
            $msm = variable_get('menu_secondary_menu', 0);
            print theme('menu_tree', $msm);
          } ?>
        </div>
      </div>

      <!-- A.4 HEADER BREADCRUMBS -->

      <!-- Breadcrumbs -->
      <div class="header-breadcrumbs">
        <ul>
        <?php
          if ($breadcrumb):
            print $breadcrumb;
            if ($title != '') {
              print " <li>$title</li>";
            }
          endif; ?>
        </ul>

        <!-- Search form -->
        <?php if ($search_box) :?>
        <div class="searchform">
          <?php print $search_box; ?>
        </div>
        <?php endif; ?>

      <?php if ($header) { print $header; } ?>

      </div>
    </div>

    <!-- For alternative headers END PASTE here -->

    <!-- B. MAIN -->
    <?php
    if (!$sidebar_left && !$sidebar_right) {
      $wide_class = ' doublewide';
    } else if (!$sidebar_left) {
      $wide_class = ' nonav';
    } else if (!$sidebar_right) {
      $wide_class = ' noblocks';
    }
    ?>
    <div class="main<?php print $wide_class; ?>">
      <!-- B.1 MAIN NAVIGATION -->
      <?php if ($sidebar_left): ?>
        <div class="main-navigation sidebar" id="sidebar-left">
          <?php print $sidebar_left ?>
        </div>
      <?php endif; ?>

      <!-- B.1 MAIN CONTENT -->
      <div class="main-content">
        <!-- Pagetitle -->
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h1'. ($tabs ? ' class="with-tabs pagetitle"' : ' class="pagetitle"') .'>'. $title .'</h1>'; endif; ?>
          <?php if ($tabs): print $tabs .'</div>'; endif; ?>

          <?php if (isset($tabs2)): print $tabs2; endif; ?>
          <!-- Content unit - One column -->
<!--           <h1 class="block"><?php //$title; ?></h1> -->
          <div class="column1-unit">
            <?php if ($help): print $help; endif; ?>
            <?php if ($messages): print $messages; endif; ?>
            <?php print $content ?>
            <span class="clear"></span>
            <?php print $feed_icons ?>
        </div>
      </div>

      <!-- B.3 SUBCONTENT -->
      <?php if ($sidebar_right): ?>
        <div class="main-subcontent sidebar" id="sidebar-right">
          <?php print $sidebar_right ?>
        </div>
      <?php endif; ?>
    </div>

    <!-- C. FOOTER AREA -->      
    <div id="footer" class="footer">
    <p><?php print $footer_message ?></p>
      <p class="credits">Design by <a href="http://www.1-2-3-4.info/" title="Designer Homepage">Wolfgang</a>  | Adapted by <a href="http://www.codingclan.com" title="CodingClan">CodingClan LLC</a> | Powered by <a href="http://drupal.org" title="Drupal">Drupal</a> | <a href="http://validator.w3.org/check?uri=referer" title="Validate XHTML code">XHTML 1.0</a> | <a href="http://jigsaw.w3.org/css-validator/" title="Validate CSS code">CSS 2.0</a></p>
    </div>
    
  </div>
  
</body>
</html>



